proDir="/root/python_full_stack"
port=8000
pid=$(netstat -nlp | grep :$port | awk '{print $7}' | awk -F"/" '{ print $1 }');
if [  -n  "$pid"  ];  then
    kill  -9  $pid;
    cd $proDir
    uwsgi --ini uwsgi.ini
else
    cd $proDir
    uwsgi --ini uwsgi.ini
fi